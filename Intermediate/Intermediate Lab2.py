#Accepting input from the user
#Accept no of elements from user
n = int(input("Enter no of elements: "))
#storing them in list a
a = []
#Using for loop to accept each element from user
for i in range(n):
    x = int(input(f"Enter {i+1} value: "))
    #Adding the elements to the list
    a.append(x)
#Accept the number to find its location in the list
find = int(input("Enter a element to find: "))
for i in range(len(a)):
    if a[i] == find:
        #Printing the output location of the number
        print(f"{find} found at location {i+1}")
