#include<stdio.h>
#include<conio.h>
void main()
{
    /*Declaring the initial values*/
    int i, a[100], n, m;
    float avg,sum=0;
    /*Accepting input(s) from users*/
    printf("Enter no of elements: "); scanf("%d",&n);
    printf("Enter %d elements:",n); 
    /*Assiging the elements to the array*/
    for(i=0;i<n;i++) scanf("%d",&a[i]);
    /*Printing the array elements*/
    printf("The array elements are:\n");
    for(i=0;i<n;i++) {printf("%4d",a[i]);}
    /*Finding the sum of the array elements*/
    for(i=0;i<n;i++) {sum = sum + a[i];}
    printf("\nThe sum of array elements is: %.f",sum);
}