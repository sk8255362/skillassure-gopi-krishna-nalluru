#include<stdio.h>
#include<conio.h>
void main()
{
    /*Accepting input(s) from user*/
    int a , b, c, x, y;
    printf("Enter a,b,c vales: "); scanf("%d%d%d",&a,&b,&c);
    /*Using terinary operator to find the smallest number*/
    x = (a < b)? a : b;
    y = (x < c)? x : c;
    /*Printing the output*/
    printf("The smallest number is: %d",y);
}