#Accepting a number from user
num = int(input("Enter a number: "))
#initializing value(s)
rev = 0
#Applying condition
while num > 0:
    rev = rev * 10 + num % 10
    num = num//10
#Printing the output
print(f"The reverse of given number is: {rev}")
