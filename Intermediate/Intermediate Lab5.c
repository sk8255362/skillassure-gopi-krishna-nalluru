#include<stdio.h>
#include<conio.h>
void main()
{
    char x;
    /*Accepting input from user*/
    printf("Enter an Alphabet: "); scanf("%c",&x);
    /*Specifing that if x is equal to vowel characters*/
    if((x==97 || x==101 || x==105 || x==111 || x==117) || (x==65 || x==69 || x==73 || x==79 || x==85))
    {
        /*Using switch cases to match the vowels*/
        switch (x)
        {
            /*Printing the case if vowels matched with input vowel characters*/
            case 65: 
            printf("%c is an vowel",x);
            break;
            case 69: 
            printf("%c is an vowel",x);
            break;
            case 73: 
            printf("%c is an vowel",x);
            break;
            case 79: 
            printf("%c is an vowel",x);
            break;
            case 85: 
            printf("%c is an vowel",x);
            break;
            case 97: 
            printf("%c is an vowel",x);
            break;
            case 101: 
            printf("%c is an vowel",x);
            break;
            case 105: 
            printf("%c is an vowel",x);
            break;
            case 111: 
            printf("%c is an vowel",x);
            break;
            case 117:
            printf("%c is an vowel",x);
            break;
        }
    }
    /*Printing consonants if input is a consonant*/
    else if((x>=65 && x<=90)||(x>=65 && x<=90))
    {
        printf("%c is consonant",x);
    }
    /*Printing space if input is a space*/
    else if(x==32 || x==" ")
    {
        printf("%c is a space",x);
    }
    /*Printing numbers if input is a number*/
    else if((x>=0 && x<=9)||(x>=48 && x<=57))
    {
        printf("%c is a number",x);
    }
    /*Printing special characters if input is a special character*/
    else
    {
        printf("%c is a special character",x);
    }
}