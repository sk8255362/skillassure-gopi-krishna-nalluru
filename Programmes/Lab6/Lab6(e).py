#Accepting input from user
n = int(input("Enter no of elements: "))
#Initializing the values
a = [1, 2]
(x, y) = (1, -2)
#Starting the series
for i in range(3, n):
    x = x+3
    a.append(x)
    y = -(-y+4)
    a.append(y)
#printing the elements upto given range
for i in range(n):
    print(a[i])
