#Taking input from user
n = int(input("Enter a number: "))
#Dividing the number with specified conditions to check even or odd
if n%2 == 0:
    print(f"{n} is a EVEN number")
else:
    print(f"{n} is an ODD number")