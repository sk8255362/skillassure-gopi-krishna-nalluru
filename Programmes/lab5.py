#Accepting inputs from user
m = int(input("Enter 1st value in range: "))
n = int(input("Enter 2nd value in range: "))
#Initilizing the vales
sum = 0
x = []
print(f"The prime numbers between {m} and {n} are: ")
#Using the nested for loop to get the prime numbers in between the range
for i in range(m, n+1):
    count = 0
    for j in range(1, i+1):
        if i % j == 0:
            count = count + 1
    if count == 2:
        print(i)
        #Appending the prime numbers to the list to find sum
        x.append(i)
#Finding the sum using for loop on list-x
for i in x:
    sum = sum + i
print(f"The sum of prim number between {m} and {n} is: {sum}")
