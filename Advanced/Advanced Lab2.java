import java.util.Scanner; 
public class Main {
    public static void main(String args[]) {
   
     //Creating Scanner object
     Scanner sc = new Scanner(System.in);
     //Accepting the count input and array elements from user
      System.out.println("Enter Count of array:");
     int x  = sc.nextInt();
     int arr[]=new int[x];
      System.out.println("Enter Array Elements:");
     for(int i=0;i<x;i++){
         arr[i]=sc.nextInt();
     }
     System.out.println("Enter Number:");
     int p=sc.nextInt();
     int c=1;
     //linear searching
     for(int i=0;i<x;i++){
         if(arr[i]==p){
           c=0;
           break;
         }
     }
     if(c==1)
     System.out.println("Number Not Found");
     else
     System.out.println("Number Found");
     
      
    }
}